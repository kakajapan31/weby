import random
import time

import youtube_dl
from selenium.webdriver.common.keys import Keys

from driver import Driver


def get_chrome_driver(data: dict):
    proxy_user = data.get('proxy_user')
    proxy_pass = data.get('proxy_pass')
    user_agent = data.get('user_agent')
    if not proxy_user or not proxy_pass:
        return Driver.get_chromedriver_proxy_without_login(proxy_url=data['proxy_url'], user_agent=user_agent,
                                                           chrome_driver=data['chrome_driver_path'])

    url = data['proxy_url']
    host, port = url.split(':')
    proxy_ = {
        'host': host,
        'port': port,
        'user': data['proxy_user'],
        'pass': data['proxy_pass'],
    }
    return Driver.get_chromedriver(use_proxy=True, proxy=proxy_, user_agent=user_agent,
                                   chrome_driver=data['chrome_driver_path'])


def get_chrome_url(chrome, data: dict):
    cookies = data.get('cookies')
    url = data['start_url']
    print(data['proxy_url'], f'go to {url}')
    if cookies:
        get_url_with_cookies(chrome, cookies, url)
        return True
    else:
        get_url_without_cookies(chrome, url)
        return False


def get_url_with_cookies(driver, cookies, url):
    driver.delete_all_cookies()
    driver.get('https://www.youtube.com/')
    for cookie in cookies:
        try:
            driver.add_cookie(cookie)
        except:
            pass
    if random.randint(0, 1):
        driver.get(url)
    else:
        get_url_by_search(driver, url)
    time.sleep(1)
    driver.maximize_window()


def get_url_without_cookies(driver, url):
    driver.get('https://www.youtube.com/')
    if random.randint(0, 1):
        driver.get(url)
    else:
        get_url_by_search(driver, url)
    time.sleep(1)
    driver.maximize_window()


def get_url_by_search(driver, url: str):
    search = driver.find_element_by_id('search')
    search.click()
    search.send_keys(Keys.CONTROL, 'a')
    search.send_keys(Keys.BACKSPACE)
    search.send_keys(url)

    time.sleep(0.5)
    driver.find_element_by_xpath('//*[@id="search-icon-legacy"]/yt-icon').click()
    time.sleep(2)
    try:
        driver.find_element_by_xpath('//*[@id="video-title"]/yt-formatted-string').click()
    except Exception as e:
        print(str(e))
        driver.get(url)


def get_length_video(url: str, default=7200):
    try:
        ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl': 'tmp/%(id)s.%(ext)s',
            'noplaylist': True,
            'quiet': True,
            'prefer_ffmpeg': True,
            'audioformat': 'wav',
            'forceduration': True
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            res = ydl.extract_info(url, download=False)
            return res['duration']
    except:
        return default


def get_list_button_next_video(driver, data: dict):
    if data.get('user_agent'):
        list_button = driver.find_elements_by_xpath(
            '//*[@id="app"]/div[2]/ytm-watch/ytm-single-column-watch-next-results-renderer/ytm-item-section-renderer[2]/lazy-list/ytm-compact-video-renderer/div/div/a/h4/span')
    else:
        list_button = driver.find_elements_by_xpath('//span[@id="video-title"]')
    if len(list_button) > 0:
        list_button.pop()
    return list_button


def get_button_next_video(driver, data: dict):
    channel_name = data['channel_name']
    list_button = get_list_button_next_video(driver, data)
    for button in list_button or []:
        button_name = button.get_attribute('aria-label')
        if button_name is None or not isinstance(button_name, str):
            continue
        if channel_name in button_name:
            return button
    return None


def get_time_view(driver, data: dict):
    time_to_see_all = data.get('time_to_see_all', 7200)
    time_view_video = 8000
    if data.get('live_stream') is not True:
        time_video = get_length_video(driver.current_url)
        time_view_video = max(time_video - 150, 0)
    if time_view_video > time_to_see_all:
        time_view_of_user = time_to_see_all
    else:
        time_view_of_user = random.randint(data['min_time_view'], data['max_time_view'])

    return min(time_view_of_user, time_view_video)


def get_button_likse_video(driver, data: dict):
    if data.get('user_agent'):
        button = driver.find_element_by_xpath(
            '//*[@id="app"]/div[2]/ytm-watch/ytm-single-column-watch-next-results-renderer/ytm-item-section-renderer[1]/lazy-list/ytm-slim-video-metadata-renderer/div[2]/c3-material-button[1]/button')
    else:
        button = driver.find_element_by_xpath(
            "//*[@id='top-level-buttons']/ytd-toggle-button-renderer[1]/a/yt-icon-button/button")
    liked = button.get_attribute('aria-pressed')

    return button, liked == 'false'


def get_button_subscribe_channel(driver, data: dict):
    need_click = False
    if data.get('user_agent'):
        button = driver.find_element_by_xpath(
            '//*[@id="app"]/div[2]/ytm-watch/ytm-single-column-watch-next-results-renderer/ytm-item-section-renderer[1]/lazy-list/ytm-slim-video-metadata-renderer/ytm-slim-owner-renderer/div/ytm-subscribe-button-renderer/div/c3-material-button/button')
        subscribed = button.get_attribute('aria-pressed')
        if subscribed == 'false':
            need_click = True
    else:
        button = driver.find_element_by_xpath('//*[@id="subscribe-button"]/ytd-subscribe-button-renderer/paper-button')
        subscribed = button.get_attribute('subscribed')
        if subscribed is None:
            need_click = True

    return button, need_click


def play_video(driver):
    try:
        for i in range(10):
            driver.execute_script('document.getElementsByTagName("video")[0].play()')
            time.sleep(0.5)
    except Exception as e:
        print(str(e))
        pass


if __name__ == '__main__':
    # print(get_length_video('https://youtu.be/bMPEkGe25Kg'))
    # print(get_length_video('https://www.youtube.com/watch?v=co5tiaufARk'))
    from selenium import webdriver

    chrome = webdriver.Chrome('../chromedriver')
    chrome.get('https://youtube.com')
    get_url_by_search(chrome, 'https://youtu.be/bMPEkGe25Kg')
