import random
import time

from helpers import get_time_view, get_button_next_video, get_button_subscribe_channel, get_button_likse_video, \
    play_video, get_url_by_search


def go_to_next_video(driver, data: dict):
    button = get_button_next_video(driver, data)
    if button is None:
        if random.randint(0, 1):
            driver.get(data['start_url'])
        else:
            get_url_by_search(driver, data['start_url'])
    else:
        button.click()


def view_video(driver, data: dict = None):
    play_video(driver)
    time_view = get_time_view(driver, data)
    print(data.get('proxy_url'), f'view video {driver.current_url} in {time_view} seconds')
    time.sleep(time_view)


def like_video(driver, data: dict = None):
    button, need_click = get_button_likse_video(driver, data)
    if need_click:
        print(data.get('proxy_url'), f'to like video {driver.current_url}')
        button.click()
        time.sleep(10)


def subscribe_channel(driver, data: dict = None):
    button, need_click = get_button_subscribe_channel(driver, data)
    if need_click:
        print(data.get('proxy_url'), 'to subscribe channel')
        button.click()
        time.sleep(10)


def comment_video(driver, comment, data: dict = None):
    for i in range(100, 800, 100):
        driver.execute_script(f"window.scrollTo(0, {i})")
        time.sleep(0.1)
    time.sleep(3)
    print(data.get('proxy_url'), f'to comment {comment}')
    driver.find_element_by_id('simplebox-placeholder').click()
    driver.find_element_by_id('contenteditable-root').send_keys(comment)
    driver.find_element_by_xpath('//*[@id="submit-button"]').click()
    time.sleep(10)
