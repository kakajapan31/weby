import pandas
import json
import codecs


if __name__ == '__main__':
    excel = pandas.read_excel('account.xlsx')
    json_str = excel.to_json(orient='records')
    list_account = json.loads(json_str)
    for i in range(len(list_account)):
        account = list_account[i]
        file = codecs.open(f'account_profile/{i}.json', 'w', 'utf-8')
        if 'proxy_user' in account:
            account['proxy_user'] = str(account['proxy_user'] or '')
        if 'proxy_pass' in account:
            account['proxy_pass'] = str(account['proxy_pass'] or '')
        if 'channel_name' in account:
            account['channel_name'] = str(account['channel_name'] or '')
        if 'start_url' in account:
            account['start_url'] = str(account['start_url'] or '')
        if 'proxy_url' in account:
            account['proxy_url'] = str(account['proxy_url'] or '')
        if 'live_stream' in account:
            account['live_stream'] = (int(account['live_stream'] or 0) == 1)
        if 'time_to_see_all' in account:
            account['time_to_see_all'] = int(account['time_to_see_all'] or 7200)
        if 'cookies' in account:
            cookies = account['cookies'] or '[]'
            account['cookies'] = json.loads(cookies)
        account['chrome_driver_path'] = '../chromedriver'

        file.write(json.dumps(account, ensure_ascii=False))
