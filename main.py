import json
import os
from multiprocessing.pool import ThreadPool

from youtube import view_video, like_video, go_to_next_video, subscribe_channel, comment_video
from helpers import get_chrome_driver, get_chrome_url


def run_chrome(data: dict):
    while True:
        chrome = get_chrome_driver(data)
        try:
            has_login = get_chrome_url(chrome, data)
            while True:
                print(data.get('proxy_url'), f'working at {chrome.current_url}')
                view_video(chrome, data)
                if has_login:
                    like_video(chrome, data)
                    subscribe_channel(chrome, data)
                    # comment_video(chrome, 'haha', data)
                go_to_next_video(chrome, data)
                print('\n' * 5)
        except:
            print(data['proxy_url'], 'run fail so quit')
        finally:
            chrome.quit()


def run_chrome_thread():
    list_data = []
    for file in os.listdir('profile'):
        if file.endswith('.json'):
            file_dir = os.path.join('profile', file)
            with open(file_dir) as json_file:
                data = json.load(json_file)
                list_data.append(data)

    pool = ThreadPool()
    pool.map(run_chrome, list_data)


if __name__ == '__main__':
    run_chrome_thread()
