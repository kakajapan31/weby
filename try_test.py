from selenium import webdriver

if __name__ == '__main__':
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument(
        '--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1')
    driver = webdriver.Chrome('../chromedriver', options=chrome_options)
    driver.get('https://www.google.com')

    chrome_options2 = webdriver.ChromeOptions()
    chrome_options2.add_argument(
        '--user-agent=Mozilla/5.0 (iPad; CPU OS 12_4_4 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) CriOS/66.0.3359.122 Mobile/16G140 Safari/604.1')
    driver2 = webdriver.Chrome('../chromedriver', options=chrome_options2)
    driver2.get('https://www.google.com')
